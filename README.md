# solr-tomcat

#### 介绍
solr集成tomcat8，集成ik中文分词器，mysql数据导入

#### 注意事项
- 需要安装Jdk8及以上
- 下载到本地以后，如果是使用tomcat,需要修改solr项目的web.xml中的solrHome为你的实际路径
- 如果是使用tomcat，日志也得修改，不修改也能跑，但是回报错，对应在webapps\solr\WEB-INF\classes\log4j2.xml
- 如果是使用tomcat，没有修改端口号 ip:8080/solr/index.html,不加无法进入控制台
- 如果使用solr内置服务器，直接在bin目录下启动服务器即可，默认端口8983